# Translation of polkit-kde-authentication-agent-1 to Croatian
#
# Andrej Dundovic <adundovi@gmail.com>, 2010.
# Marko Dimjašević <marko@dimjasevic.net>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-31 01:57+0000\n"
"PO-Revision-Date: 2011-06-28 15:19+0200\n"
"Last-Translator: Marko Dimjašević <marko@dimjasevic.net>\n"
"Language-Team: Croatian <kde-croatia-list@lists.sourceforge.net>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Marko Dimjašević"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "marko@dimjasevic.net"

#: IdentitiesModel.cpp:27
#, kde-format
msgid "Select User"
msgstr "Odaberite korisnika"

#: IdentitiesModel.cpp:45
#, kde-format
msgctxt "%1 is the full user name, %2 is the user login name"
msgid "%1 (%2)"
msgstr ""

#: main.cpp:48
#, kde-format
msgid "(c) 2009 Red Hat, Inc."
msgstr "© 2009 Red Hat, Inc."

#: main.cpp:49
#, kde-format
msgid "Lukáš Tinkl"
msgstr ""

#: main.cpp:49
#, kde-format
msgid "Maintainer"
msgstr "Održavatelj"

#: main.cpp:50
#, kde-format
msgid "Jaroslav Reznik"
msgstr "Jaroslav Reznik"

#: main.cpp:50
#, fuzzy, kde-format
#| msgid "Maintainer"
msgid "Former maintainer"
msgstr "Održavatelj"

#: policykitlistener.cpp:71
#, kde-format
msgid "Another client is already authenticating, please try again later."
msgstr "Drugi klijent se trenutno prijavljuje, molim pokušajte kasnije."

#~ msgid "Action:"
#~ msgstr "Radnja:"

#~ msgid "Vendor:"
#~ msgstr "Izvođač:"

#, fuzzy
#~| msgid "Action:"
#~ msgid "Action ID:"
#~ msgstr "Radnja:"

#~ msgid "Password for root:"
#~ msgstr "Zaporka za root-a:"

#~ msgid "Password for %1:"
#~ msgstr "Zaporka za %1:"

#~ msgid "Password:"
#~ msgstr "Zaporka:"

#~ msgid "Password or swipe finger for root:"
#~ msgstr "Zaporka ili prođite prstom za root korisnika:"

#~ msgid "Password or swipe finger for %1:"
#~ msgstr "Zaporka ili prođite prstom za %1:"

#~ msgid "Password or swipe finger:"
#~ msgstr "Zaporka ili prođite prstom:"

#~ msgid ""
#~ "An application is attempting to perform an action that requires "
#~ "privileges. Authentication is required to perform this action."
#~ msgstr ""
#~ "Aplikacija pokušava izvesti radnju koja zahtijeva ovlasti. Potrebna je "
#~ "autentifikacija za izvođenje te radnje."

#~ msgid "Authentication failure, please try again."
#~ msgstr "Neuspjela autentifikacija. Pokušajte ponovo."

#~ msgctxt ""
#~ "%1 is the name of a detail about the current action provided by polkit"
#~ msgid "%1:"
#~ msgstr "%1:"

#~ msgid "Click to open %1"
#~ msgstr "Kliknite da biste otvorili %1"

#, fuzzy
#~| msgid "Password:"
#~ msgid "P&assword:"
#~ msgstr "Zaporka:"

#, fuzzy
#~| msgid "PolicyKit1-KDE"
#~ msgid "PolicyKit1 KDE Agent"
#~ msgstr "PolicyKit1-KDE"

#~ msgid "Application:"
#~ msgstr "Aplikacija:"

#~ msgid "Click to edit %1"
#~ msgstr "Kliknite da biste uredili %1"

#~ msgid "Switch to dialog"
#~ msgstr "Prebaci na dijaloški prozor"

#~ msgid "Cancel"
#~ msgstr "Odustani"

#~ msgid "Remember authorization"
#~ msgstr "Zapamti autorizaciju"

#~ msgid "For this session only"
#~ msgstr "Samo za ovu sesiju"
