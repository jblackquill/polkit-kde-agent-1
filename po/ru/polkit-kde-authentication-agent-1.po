# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Yuri Efremov <yur.arh@gmail.com>, 2010, 2011.
# Alexander Potashev <aspotashev@gmail.com>, 2010, 2015, 2018.
# Alexander Yavorsky <kekcuha@gmail.com>, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-31 01:57+0000\n"
"PO-Revision-Date: 2019-01-13 21:55+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 18.12.1\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Александр Мелентьев,Николай Шафоростов,Юрий Ефремов,Ольга Миронова"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"alex239@gmail.com,shaforostoff@kde.ru,yur.arh@gmail.com,omiro@basealt.ru"

#: IdentitiesModel.cpp:27
#, kde-format
msgid "Select User"
msgstr "Выберите пользователя"

#: IdentitiesModel.cpp:45
#, kde-format
msgctxt "%1 is the full user name, %2 is the user login name"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: main.cpp:48
#, kde-format
msgid "(c) 2009 Red Hat, Inc."
msgstr "© Red Hat, Inc., 2009"

#: main.cpp:49
#, kde-format
msgid "Lukáš Tinkl"
msgstr "Lukáš Tinkl"

#: main.cpp:49
#, kde-format
msgid "Maintainer"
msgstr "Сопровождающий"

#: main.cpp:50
#, kde-format
msgid "Jaroslav Reznik"
msgstr "Jaroslav Reznik"

#: main.cpp:50
#, kde-format
msgid "Former maintainer"
msgstr "Прежний сопровождающий"

#: policykitlistener.cpp:71
#, kde-format
msgid "Another client is already authenticating, please try again later."
msgstr ""
"Аутентификация уже выполняется другим процессом, повторите попытку позже."

#~ msgid "Action:"
#~ msgstr "Действие:"

#~ msgid "<null>"
#~ msgstr "<null>"

#~ msgid "Vendor:"
#~ msgstr "Поставщик:"

#~ msgid "Action ID:"
#~ msgstr "Идентификатор действия:"

#~ msgid "ID:"
#~ msgstr "Идентификатор"

#~ msgid "Details"
#~ msgstr "Подробности"

#~ msgid "Authentication Required"
#~ msgstr "Требуется аутентификация"

#~ msgid "Password for root:"
#~ msgstr "Пароль для root:"

#~ msgid "Password for %1:"
#~ msgstr "Пароль для %1:"

#~ msgid "Password:"
#~ msgstr "Пароль:"

#~ msgid "Password or swipe finger for root:"
#~ msgstr "Введите пароль или приложите палец для получения root-доступа:"

#~ msgid "Password or swipe finger for %1:"
#~ msgstr "Введите пароль или приложите палец для %1:"

#~ msgid "Password or swipe finger:"
#~ msgstr "Введите пароль или приложите палец:"

#~ msgid ""
#~ "An application is attempting to perform an action that requires "
#~ "privileges. Authentication is required to perform this action."
#~ msgstr ""
#~ "Приложение пытается выполнить действие, которое требует дополнительных "
#~ "привилегий. Для этого требуется аутентификация."

#~ msgid "Authentication failure, please try again."
#~ msgstr "Ошибка аутентификации, попробуйте ещё раз."

#~ msgctxt ""
#~ "%1 is the name of a detail about the current action provided by polkit"
#~ msgid "%1:"
#~ msgstr "%1:"

#~ msgid "'Description' not provided"
#~ msgstr "Поле «Описание» не содержит данных"

#~ msgid "Click to open %1"
#~ msgstr "Нажмите, чтобы открыть %1"

#~ msgid "P&assword:"
#~ msgstr "П&ароль:"

#~ msgid "PolicyKit1 KDE Agent"
#~ msgstr "Агент PolicyKit1 от KDE"

#~ msgid "Application:"
#~ msgstr "Приложение:"

#~ msgid "Click to edit %1"
#~ msgstr "Нажмите, чтобы изменить %1"

#~ msgid "Switch to dialog"
#~ msgstr "Переключиться на диалоговое окно"

#~ msgid "Cancel"
#~ msgstr "Отмена"

#~ msgid "Remember authorization"
#~ msgstr "Запомнить данные авторизации"

#~ msgid "For this session only"
#~ msgstr "Только для данного сеанса"
